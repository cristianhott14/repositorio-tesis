import serial, time
import driver_func as dr
import csv_register


if __name__=="__main__":
    print("PRUEBA CONTROL LAZO DE CORRIENTE")

    # Se inicia la comunicacion por el puerto serial
    ser = serial.Serial('COM3', baudrate=9600, timeout= 0.1)
    print("Iniciando comunicacion con Drive")

    # Se configura la comunicación con el drive
    dr.comm_ser(ser)

    # Establece la corriente de salida a 0 A
    dr.SendAsciiCmd(ser, 's r0x02 0')

    # Ingreso de la corriente de prueba
    while True:
        try:
            corr_deseada_A = float(input("Ingresar corriente de prueba entre -1 a 1 A: "))
            T_prueba_deseado = int(input("Ingresar tiempo de prueba en segundos: "))
            if corr_deseada_A >= -1 and corr_deseada_A <= 1 and T_prueba_deseado > 1:
                break
            else:
                print("Al menos uno de los valores no cumple con las condiciones. Intente de nuevo.")
        except ValueError:
            print("Error al ingresar un valor no válido. Intente de nuevo.")

    # Convertir corriente de A a 0.01 A
    corr_deseada = str(int(corr_deseada_A*100))
    print("Corriente deseada x10^-2 A: ",corr_deseada)

    # Habilitar el drive en el modo de corriente programada
    dr.SendAsciiCmd(ser, 's r0x24 1')

    # Se llama a la funcion de registro en csv
    registrador = csv_register.CSV(None,corr_deseada_A,T_prueba_deseado, None,'current loop')
    
    # Variable auxiliar para cambiar la corriente comando
    estado = False

    # Se inicia el tiempo de prueba
    T_prueba_inicio = time.time()

    try:
        while True:
            # Tiempo de prueba actual
            T_prueba_actual = round(time.time()-T_prueba_inicio,3)
            #print(T_prueba_actual)

            # Velocidad actual del motor
            V_actual = dr.SendAsciiCmd(ser, 'g r0x18')  

            # Registro de tiempo de prueba y monitoreo de velocidades en csv
            registrador.fill_csv_current_loop(T_prueba_actual, V_actual)        
             
            # Condicion para iniciar la prueba/ cambiar la corriente en el segundo 1
            if T_prueba_actual >=1 and not estado:
                # Establece la corriente del motor a la corriente deseada. 
                dr.SendAsciiCmd(ser, 's r0x02 '+corr_deseada)
                estado = True       # auxiliar para que este condicional no se vuelva a repertir
            
            # Cuando se supere el timepo de prueba
            if T_prueba_actual>=1+T_prueba_deseado:
                # Establece la corriente del motor a 0. 
                dr.SendAsciiCmd(ser, 's r0x02 0')

                # Se desactiva el drive
                dr.SendAsciiCmd(ser, 's r0x24 0') 
                
                # Se cierra la comunicacion
                ser.close()
                print("Prueba finalizada.")
                break

    # Se puede interrumpir el programa con "ctrl+c"   
    except(KeyboardInterrupt,SystemExit):       
        # Se desactiva el drive
        dr.SendAsciiCmd(ser, 's r0x24 0') 
                
        # Se cierra la comunicacion
        ser.close()
        print("Prueba cancelada.")