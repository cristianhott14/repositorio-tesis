import time

# Envio de comando ASCII y devuelve la respuesta del drive
def SendAsciiCmd(ser, cmd):
    cmd = cmd + '\r'
    ser.write(cmd.encode())
    #print("ASCII Cmd Sent: " + cmd)
    return SerReadASCIIResponse(ser)

# Recopila todas las respuestas ASCII de la unidad
def SerReadASCIIResponse(ser):
    ret = ser.readline()
    if(len(ret) > 0):
        retStr = str(ret) # "b'ok\r" es la respuesta típica. Se elimina todo menos "ok"
        retStr = retStr[2:(len(retStr) - 3)]  # Cortar la 'b al principio y el retorno de carro (\r) al final
        #print("ASCII Response: " + retStr)
        return(retStr)

def comm_ser(ser):
    # Obtener la velocidad en baudios
    Baudios = SendAsciiCmd(ser, 'g r0x90')
    while Baudios == None:
        print('Drive apagado, por favor encienda el drive.')
        time.sleep(5)

    ser.break_condition = True

    # sleep por 50 milisegundos
    time.sleep(0.05)

    ser.break_condition = False

    # Vaciar puerto serie
    ser.flush()

    # Enviar comndo ASCII la velocidad en baudios en el drive
    SendAsciiCmd(ser, 's r0x90 115200')

    # sleep por 200 milisegundos
    time.sleep(0.2)

    # Vaciar puerto serie
    ser.flush()

    # Actualizar la velocidad en baudios
    ser.baudrate = 115200

    # Obtener la velocidad en baudios para comprbar el cambio
    SendAsciiCmd(ser, 'g r0x90')

    # Desactivar el drive
    SendAsciiCmd(ser, 's r0x24 0') 

    # Borrar registro de estado de fallo enclavado
    SendAsciiCmd(ser, 's r0xa4 0xffff')

    # Indica estado
    print("Drive operativo.\n")