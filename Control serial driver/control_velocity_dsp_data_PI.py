import serial, time
import driver_func as dr
import csv_register

# Parametros PIs orignales
'''
PIs = [['CME','13005','2890'],
    ['Z-N','6794','13649'],
    ['Miller','5961','11975'],
    ['Smith','6633','16778'],
    ['Alfaro','6594','16597'],
    ['Broida','6590','16527'],
    ['Ch-Yng','5748','12718'],
    ['Ho','5969','13839'],
    ['Viteckova','6574','16484'],
    ['Original','500','150']]
'''

# Parametros PIs nominales

PIs = [['CME','18392','4087'],
    ['Z-N','7153','13631'],
    ['Miller','5747','10953'],
    ['Smith','5746','12613'],
    ['Alfaro','5716','12492'],
    ['Broida','5716','12453'],
    ['Ch-Yng','5065','9890'],
    ['Ho','5223','10614'],
    ['Viteckova','5702','12420']]


# Parametros PIs experimentales
'''
PIs = [['CME','17034','3786'],
    ['Z-N','5826','9282'],
    ['Miller','4628','7374'],
    ['Smith','5030','10225'],
    ['Alfaro','5008','10140'],
    ['Broida','5009','10115'],
    ['Ch-Yng','4497','8244'],
    ['Ho','4610','8746'],
    ['Viteckova','4996','10089']]
'''

if __name__=="__main__":
    print("PRUEBA CONTROL DE LAZO DE VELOCIDAD CON DIFERENTES VALORES DE VP Y VI")
    # Se inicia la comunicacion por el puerto serial
    ser = serial.Serial('COM3', baudrate=9600, timeout= 0.1)
    print("Iniciando comunicacion con Drive")

    # Se configura la comunicación con el drive
    dr.comm_ser(ser)

    # Establece la velocidad del motor en 0 conteos/segundo. 5820 conteos es una vuelta. Seria cps /58200 * 60 a rpm. 
    dr.SendAsciiCmd(ser, 's r0x2f 0')

    # Ingreso de la velocidad y tiempo de prueba
    while True:
        try:
            vel_deseada_rpm = int(input("Ingresar velocidad de prueba entre -200 a 200 rpm: "))
            T_prueba_deseado = int(input("Ingresar tiempo de prueba en segundos: "))
            Metodo = int(input("Seleccione el metodo a utilizar (0 a 8): "))
            if vel_deseada_rpm >= -200 and vel_deseada_rpm <= 200 and T_prueba_deseado > 1 and Metodo>=0 and Metodo<=8:
                break
            else:
                print("Al menos uno de los valores no cumple con las condiciones. Intente de nuevo.")
        except ValueError:
            print("Error al ingresar un valor no válido. Intente de nuevo.")

    # Conversión de rpm a cps
    vel_deseada_cps= str(int(vel_deseada_rpm/60*58200))
    #print("Velocidad en cps: ",vel_deseada_cps,"\n")

    # Imprimir el metodo a seleccionado y sus parametros Vp y Vi
    print("Metodo de",PIs[Metodo][0])
    Vp_new = PIs[Metodo][1]
    Vi_new = PIs[Metodo][2]
    # Establecer Vp y Vi en el drive
    dr.SendAsciiCmd(ser, 's r0x27 '+ Vp_new)
    dr.SendAsciiCmd(ser, 's r0x28 '+ Vi_new)

    # Comprobar valores establecidos
    Vp_new = dr.SendAsciiCmd(ser, 'g r0x27')
    Vi_new = dr.SendAsciiCmd(ser, 'g r0x28')
    print("Nuevo Vp:", Vp_new[1:])
    print("Nuevo Vi:", Vi_new[1:])

    time.sleep(2)

    # Habilitar el drive en el modo de velocidad programada 
    dr.SendAsciiCmd(ser, 's r0x24 11')

    # Se llama a la funcion de registro en csv
    registrador = csv_register.CSV(vel_deseada_rpm,None,T_prueba_deseado, Metodo,'velocity loop')

    # Variable axuliar para cambiar la velocidad del motor
    estado = False
    
    # Se inicia el tiempo de prueba
    T_prueba_inicio = time.time()

    try:
        while True:
            # Tiempo de prueba actual
            T_prueba_actual = round(time.time()-T_prueba_inicio,3)
            print(T_prueba_actual)

            # Velocidad actual del motor
            #V_actual = dr.SendAsciiCmd(ser, 'g r0x18')  
            
            # Error de velocidad del motor
            V_error = dr.SendAsciiCmd(ser, 'g r0x2a') 

            # Registro de tiempo de prueba y monitoreo de error de velocidad en csv 
            registrador.fill_csv_velocity_loop(T_prueba_actual, V_error)  

            # Condicion para iniciar la prueba en el segundo 1
            if T_prueba_actual>=1 and not estado:
                # Establece la velocidad del motor en la velocidad de prueba.
                dr.SendAsciiCmd(ser, 's r0x2f '+vel_deseada_cps)
                estado = True   # Cambio de estado para que la condición solo se repita una vez dentro del bucle while
            
            # Cuando se supere el timepo de prueba:
            if T_prueba_actual>=1+T_prueba_deseado:
                # Establece la velocidad del motor en 0 conteos/segundo. 
                dr.SendAsciiCmd(ser, 's r0x2f 0')

                # Se desactiva el drive
                dr.SendAsciiCmd(ser, 's r0x24 0')

                # Establece Vp original
                dr.SendAsciiCmd(ser, 's r0x27 17394')

                # Establece Vi original
                dr.SendAsciiCmd(ser, 's r0x28 3865')
                
                # Se cierra la comunicacion
                ser.close()
                
                print("Prueba finalizada.")
                break

    #Se puede interrumpir el programa con "ctrl+c"   
    except(KeyboardInterrupt,SystemExit):       
        # Se desactiva el drive
        dr.SendAsciiCmd(ser, 's r0x24 0') 

        # Establece Vp original
        dr.SendAsciiCmd(ser, 's r0x27 17394')

        # Establece Vi original
        dr.SendAsciiCmd(ser, 's r0x28 3865')
                
        # Se cierra la comunicacion
        ser.close()
        print("Prueba cancelada.")