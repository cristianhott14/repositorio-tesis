import numpy as np
import pandas as pd
import glob,os

# Convertir cps a rpm
def cps_rpm(x):
    return x/58200*60

# Convertir a velocidad actual
def v_actual(error,velocidad_prueba):
    return velocidad_prueba-error

# Funcion para promediar cada 3 elementos
def promediar(lista):
    return [round(np.sum(lista[i:i+3])/3,3) for i in range(0, len(lista), 3)]

# Ordenar resultados por nombre de metodo
def analisis(data):
    # Nombre de metodos
    metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova","Original"]

    # Combinar las listas Ise_data y metodos en una lista de tuplas
    tuplas_data = list(zip(data, metodos))

    # Ordenar la lista de tuplas por el valor de Ise
    tuplas_ordenadas = sorted(tuplas_data, key=lambda tup: tup[0])
    #metodos_ordenados = [tupla[1] for tupla in tuplas_ordenadas]

    # Separar la lista ordenada en dos listas diferentes
    data_ordenado, metodos_ordenados = zip(*tuplas_ordenadas)

    # Obtener procentaje de error en referencia al metodo con mejor resultados para cada indice de desempeño
    error_porcentual_ordenado = []
    for valor_actual in data_ordenado:
        error_porcentual = round((valor_actual - data_ordenado[0]) / data_ordenado[0] * 100,2)
        error_porcentual_ordenado.append(error_porcentual)

    print("Metodos:",metodos_ordenados)
    print("Valor:",data_ordenado,"\n")
    #print("Error %:",error_porcentual_ordenado,"\n")


def indice_desempeno(ruta):
    # Buscar todos los archivos CSV en la carpeta actual
    archivos_csv = glob.glob(ruta + "*.csv")

    # Ordenar la lista de archivos CSV en función del número en el nombre de archivo
    archivos_csv = sorted(archivos_csv, key=lambda x: int(os.path.basename(x).split()[0]))

    prom_error_data = []
    desv_est_data = []
    Ise_data = []
    Itse_data = []
    Iae_data = []
    Itae_data = []

    for archivo in archivos_csv:
        # Velocidad de prueba 50 o 150
        v_prueba = int(ruta.split("/")[-2].rstrip(" rpm/"))
        
        # Leer los datos desde un archivo csv
        df = pd.read_csv(archivo)

        # Obtener los valores de x e y a partir de los datos
        x = df['Tiempo']
        df['V error'] = df['V error'].apply(cps_rpm)      # conversion cps a rpm
        y = df['V error']

        # Cálculo velocidad actual
        df['V actual'] = df.loc[df.index >= 27, 'V error'].apply(v_actual, args=(v_prueba,))
        y2= df['V actual']

        # Calculo de la desviación estándar de la velocidad
        desv_est = y2.std()

        # Calculo de promedio en valor absoluto del error de velocidad
        promedio = y.abs().mean()
        prom_error_porc = promedio/v_prueba*100

        # Calculo de tiempo de muestreo Ts
        sum=0
        for i in range(0,len(x)-1):
            dif = x[i+1]-x[i]
            sum = dif + sum
        Ts = sum/len(x)/60
        #print("Ts: ",Ts)

        # Calculo de los indices de desempeño
        Ise = Itse = Iae = Itae = 0
        for j in range(27,552):
            aux = abs((y[j] + y[j-1])/2 *Ts)
            Iae = Iae + aux
            aux2 = aux*j
            Itae = Itae + aux2
            aux3 = ((y[j])**2 + (y[j-1])**2)/2 *Ts
            Ise = Ise + aux3
            aux4 = aux3*j
            Itse = Itse + aux4
        
        # Almacenar lo valores de cada archivo
        prom_error_data.append(prom_error_porc)
        desv_est_data.append(desv_est)
        Iae_data.append(Iae)
        Itae_data.append(Itae)
        Ise_data.append(Ise)
        Itse_data.append(Itse)

    # Promediar valores de los 3 archivos
    prom_error_data = promediar(prom_error_data)
    desv_est_data = promediar(desv_est_data)
    Iae_data = promediar(Iae_data)
    Itae_data = promediar(Itae_data)
    Ise_data = promediar(Ise_data)
    Itse_data = promediar(Itse_data)
   
    print("Promedio error porcentual en orden")
    prom_error_orden = analisis(prom_error_data)

    print("Desviación estandar de la velocidad en orden")
    desv_est_orden = analisis(desv_est_data)

    print("IAE en orden")
    Iae_orden = analisis(Iae_data)

    print("ITAE en orden")
    Itae_orden = analisis(Itae_data)

    print("ISE en orden")
    Ise_orden = analisis(Ise_data)

    print("ITSE en orden")
    Itse_orden = analisis(Itse_data)

# Lectura de las diferentes carpetas
carpetas_principales = ["Valores originales", "Valores nominales", "Valores experimentales"]
subcarpetas = ["50 rpm", "150 rpm"]

for subcarpeta in subcarpetas:
    for carpeta_principal in carpetas_principales:
        # Generar ruta de carpetas
        ruta = f"csv/velocity_loop/{carpeta_principal}/{subcarpeta}/"  
        
        # Generar tiulos por carpeta
        id = carpeta_principal + " " + subcarpeta
        id = id.center(100, " ")
        seperador = "="*len(id)
        print(seperador)
        print(id)
        print(seperador)

        # Calculo de los indices de desempeño
        indice_desempeno(ruta)  