import glob
import os
import pandas as pd

# Obtener la lista de archivos CSV en la carpeta
csv_files = glob.glob('*.csv')

# Ordenar los archivos por fecha de modificación (el más reciente primero)
csv_files.sort(key=lambda x: os.path.getmtime(x), reverse=True)

# Leer el último archivo CSV de la lista
ultimo_archivo = csv_files[0]
df = pd.read_csv(ultimo_archivo)

# Obtener los valores de  corriente comando
i_comando = df.loc[20:,'I comando']

# Calcular el promedio de corriente comando
promedio_i = round(i_comando.mean()/100,3)

print("Promedio de corriente comando:", promedio_i)
