import glob, os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


# Obtener la lista de archivos CSV en la carpeta
csv_files = glob.glob('*.csv')

# Ordenar los archivos por fecha de modificación (el más reciente primero)
csv_files.sort(key=lambda x: os.path.getmtime(x), reverse=True)

# Leer el último archivo CSV de la lista
ultimo_archivo = csv_files[0]
df = pd.read_csv(ultimo_archivo)
# Leer los datos desde un archivo csv
#df = pd.read_csv('0.67 A 10s Tue May 23 11-55-55 2023.csv')

# Convertir cps a rpm
def cps_rpm(x):
    return x/58200*60

# Definir la función a ajustar
def y_func(x, K, tau, L):
    return K*(1 - np.exp(-(x-L)/tau))

# Obtener los valores de x e y a partir de los datos
x = df.loc[10:,'Tiempo']
df['V actual'] = df['V actual'].apply(cps_rpm)      # conversion
y = df.loc[10:,'V actual']

# Ajustar la función a los datos
popt, pcov = curve_fit(y_func, x, y, p0=[100, 1, 1])

# Obtener los valores de K, tau y L ajustados
K = popt[0]
tau = popt[1]
L = popt[2]
print("K: ",K,"tau: ",tau,"L: ",L-1)

# Calcular los valores para la curva ajustada
y_fit = y_func(x, K, tau, L)
t = np.linspace(0,max(x),100)
func_aprox = y_func(t, K, tau, L)

# Tangente manual
L_zn = L + 0.03
m = 99  # pendiente recta manual
recta_tan = m*(t-L_zn)
tau_zn = K/m+L_zn


plt.plot(x, y,label="Datos")       # Datos obtenidos
#plt.plot(t, func_aprox, 'r',label="Aprox. datos")   # Funcion aproximada
plt.plot(t, np.ones_like(t)*K,'--g',label="K promedio")  # Vector de K
plt.plot(t,recta_tan,'--k',label="Rec. tangente")   # Recta tangente
plt.axvline(x=tau_zn, ymin=0, ymax=K, color="brown", linestyle="--",label="Rec. tau Z-N")   # Vector para tau
plt.ylim(0,K+10)
plt.xlim(0,max(x))
# Configurar el tamaño de los ticks
plt.gca().xaxis.set_major_locator(plt.MultipleLocator(1))
plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(0.1))
plt.tick_params(axis='both', which='major', labelsize=10)
plt.tick_params(axis='both', which='minor', labelsize=9)
plt.title('Respuesta del sistema a entrada escalon')
plt.xlabel('Tiempo(s)')
plt.ylabel('Velocidad (rpm)')
plt.legend(loc = "lower right")
plt.show()


# Calculos valores de PI para el lazo de velocidad
def PI(L,tau):
    Kp = round(0.9*tau/(L),3)
    Ki = round(Kp/(L/0.3),3)
    print("Kp: ", Kp, "Ki: ", Ki)
    KpCME = round(Kp/60*58200)
    KiCME = round(Ki/60*58200)
    print("CME Kp: ", KpCME, "Ki: ", KiCME,"\n")
    
L = L-1 # Ajuste por inicio de prueba en segundo 1

# Metodos de la recta tangente
# Ziegler Nichols
print("Ziegler Nichols")
tau_zn = tau_zn - 1     # Ajuste inicio de prueba en 1 segundo
L_zn = L_zn - 1
PI_ZN = PI(L_zn,tau_zn)

# Miller
print("Miller")
t_632 = L - tau * np.log(0.368)
PI_Mll = PI(L_zn,t_632)

# Metodos de dos puntos
# Smith
print("Smith")
t_283 = L - tau * np.log(0.717)
tau_smith = -1.5*t_283 + 1.5*t_632
L_smith = 1.5*t_283 - 0.5*t_632
PI_Smith = PI(L_smith,tau_smith)

# Alfaro
print("Alfaro")
t_25 = L - tau * np.log(0.75)
t_75 = L - tau * np.log(0.25)
tau_alf = -0.91*t_25 + 0.91*t_75
L_alf = 1.262*t_25 - 0.262*t_75
PI_Alfaro = PI(L_alf,tau_alf)

# Broida
print("Broida")
t_28 = L - tau * np.log(0.72)
t_40 = L - tau * np.log(0.60)
tau_brda = -5.5*t_28 + 5.5*t_40
L_brda = 2.8*t_28 - 1.8*t_40
PI_Broida = PI(L_brda,tau_brda)

# Chen y Yang
print("Chen y Yang")
t_33 = L - tau * np.log(0.67)
t_67 = L - tau * np.log(0.33)
tau_chyg = -1.4*t_33 + 1.4*t_67
L_chyg = 1.54*t_33 - 0.54*t_67
PI_Chen_Yang = PI(L_chyg,tau_chyg)

# Ho
print("Ho")
t_35 = L - tau * np.log(0.65)
t_85 = L - tau * np.log(0.15)
tau_ho = -0.67*t_35 + 0.67*t_85
L_ho = 1.3*t_35 - 0.29*t_85
PI_Ho = PI(L_ho,tau_ho)

# Vitecková
print("Viteckova")
t_70 = L - tau * np.log(0.30)
tau_vck = -1.245*t_33 + 1.245*t_70
L_vck = 1.498*t_33 - 0.498*t_70
PI_Viteckova = PI(L_vck,tau_vck)