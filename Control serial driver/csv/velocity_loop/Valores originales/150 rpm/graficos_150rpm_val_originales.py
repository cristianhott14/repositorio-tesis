import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

# Obtener la lista de archivos CSV en la carpeta actual
csv_files = glob.glob('*.csv')

# Ordenar la lista de archivos CSV en función del número en el nombre de archivo
csv_files = sorted(csv_files, key=lambda x: int(x.split()[0]))

# Nombre de métodos
metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova", "Original"]

# Paleta de colores personalizada
colores = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#000000']

# Asignar nombres de métodos a los archivos
nombres_metodos = {}
for i, archivo in enumerate(csv_files):
    n_prueba = (archivo.split(" ")[0])     # Obtener el numero de prueba
    metodo = metodos[int(n_prueba)]  # Obtener el nombre de método correspondiente al numero de prueba
    nombres_metodos[archivo] = metodo

# Procesar cada archivo CSV
for archivo in csv_files:
    # Leer el archivo CSV
    df = pd.read_csv(archivo)

    # Convertir cps a rpm
    def cps_rpm(x):
        return x / 58200 * 60

    # Obtener los valores de x e y a partir de los datos
    x = df['Tiempo']
    y = df['V error'].apply(cps_rpm)  # Convertir los valores de "V error" de cps a rpm

    # Cálculo velocidad actual
    for i in range(11, len(y)):
        y[i] = 150 - y[i]

    # Obtener el nombre del método a partir del diccionario
    nombre_metodo = nombres_metodos[archivo]

    # Obtener el color del método en el orden correcto
    color = next(color for method, color in zip(metodos, colores) if method == nombre_metodo)

    # Graficar los datos del archivo CSV actual con el nombre del método y color
    plt.plot(x, y, label=nombre_metodo, color=color)

# Configurar el aspecto de la gráfica
plt.xlim(1, 10)  # Establecer los límites del eje x
plt.ylim(145, 167)  # Establecer los límites del eje y
plt.xlabel('Tiempo (s)')
plt.ylabel('Velocidad actual (rpm)')
plt.title('Gráfico de velocidad de las pruebas a 150 rpm')

# Crear etiquetas personalizadas para la leyenda con los métodos y colores correspondientes
etiquetas = [Line2D([0], [0], color=color, label=metodo) for metodo, color in zip(metodos, colores)]

# Configurar la leyenda con las etiquetas personalizadas
plt.legend(handles=etiquetas, loc='center left', bbox_to_anchor=(1, 0.5))

# Ajustar los márgenes para asegurar que la leyenda se muestra completamente
plt.subplots_adjust(right=0.8)

# Configurar el tamaño de los ticks
plt.gca().yaxis.set_major_locator(plt.MultipleLocator(1))
plt.gca().yaxis.set_minor_locator(plt.MultipleLocator(1))
plt.tick_params(axis='both', which='major', labelsize=8)
plt.tick_params(axis='both', which='minor', labelsize=8)


# Configurar el tamaño de los ticks
plt.gca().xaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(0.1))
plt.tick_params(axis='both', which='major', labelsize=8)
plt.tick_params(axis='both', which='minor', labelsize=8)

# Datos de las rectas horizontales
y = [166.3]
y2 = [152.8,147.2]
y3 = [151.8,148.1]
x = 10
labels = ['V peak','V max org.','V min org.','V max','V min']

# Crear el gráfico
plt.hlines(y, 0, x, "r",linestyle = 'dotted')
plt.hlines(y2, 0, x,"b",linestyle = 'dashed')
plt.hlines(y3, 0, x,"b",linestyle = 'dotted')

# Etiquetas en el eje y
ticks = [145, 150, 155, 160, 165,167] + y + y2 + y3
labels = ['145', '150', '155', '160','165','167'] + labels
plt.yticks(ticks, labels)
plt.grid()
plt.show()