import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

#archivo = '9 50 rpm 60s Tue May 23 12-57-53 2023.csv'
archivo = '9 50 rpm 60s Tue May 23 12-59-07 2023.csv'
#archivo = '9 50 rpm 60s Tue May 23 13-00-22 2023.csv'

# Obtener la velocidad de prueba del archivo
v_prueba = int(archivo.split(" ")[1])

# Convertir cps a rpm
def cps_rpm(x):
    return x/58200*60

# Convertir a velocidad actual
def v_actual(error,velocidad_prueba):
    return velocidad_prueba-error

# Leer el archivo CSV
df = pd.read_csv(archivo)

# Obtener los valores
x = df['Tiempo']
# Conversion cps a rpm
df['V error'] = df['V error'].apply(cps_rpm) 
# Cálculo velocidad actual
df['V actual'] = df.loc[df.index >= 27, 'V error'].apply(v_actual, args=(v_prueba,))

# Extraer valores despues del sobreimpulso
y = df.loc[27:538,'V actual'].values #538

# Calculo de tiempo de muestreo Ts
sum=0
for i in range(0,len(x)-1):
    dif = x[i+1]-x[i]
    sum = dif + sum
Ts = sum/len(x)
#print("Ts: ",Ts)

N = len(y)  # tamano de datos
print(N)
frec = fftfreq(N, Ts)[:N//2]  # Calcular las frecuencias correspondientes a los coeficientes de Fourier
fourier1 = fft(y)

w = np.blackman(N)
# Transformada de Fourier
fourier2 = fft(y*w)

#plt.semilogy(frec[1:N//2], 2.0/N * np.abs(fourier1[1:N//2]), '-b')
plt.semilogy(frec[1:N//2], 2.0/N * np.abs(fourier2[1:N//2]), '-b')
plt.xlim(0,4.6)
plt.legend(['FFT', 'FFT w. window'])
plt.title('Análisis espectral '+ str(v_prueba) + ' rpm con valores originales de planta')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Magnitud')
# Configurar el tamaño de los ticks
plt.gca().xaxis.set_major_locator(plt.MultipleLocator(0.5))
plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(0.1))
plt.tick_params(axis='both', which='major', labelsize=10)
plt.tick_params(axis='both', which='minor', labelsize=9)
#plt.axvline(x=0.825, ymin=0, ymax=0.36, color= 'r' , linestyle="--") 
plt.axvline(x=1.668, ymin=0, ymax=0.59, color='r', linestyle="--")  
plt.grid()
plt.show()