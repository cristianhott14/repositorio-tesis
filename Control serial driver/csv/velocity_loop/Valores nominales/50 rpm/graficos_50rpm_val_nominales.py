import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

# Obtener la lista de archivos CSV en la carpeta actual
csv_files = glob.glob('*.csv')

# Ordenar la lista de archivos CSV en función del número en el nombre de archivo
csv_files = sorted(csv_files, key=lambda x: int(x.split()[0]))

# Nombre de métodos
metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova"]

# Paleta de colores personalizada
colores = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22']

# Asignar nombres de métodos a los archivos
nombres_metodos = {}
colores_metodos = {}
for i, archivo in enumerate(csv_files):
    n_prueba = (archivo.split(" ")[0])     # Obtener el numero de prueba
    metodo = metodos[int(n_prueba)]  # Obtener el nombre de método correspondiente al numero de prueba
    nombres_metodos[archivo] = metodo

# Procesar cada archivo CSV
for archivo in csv_files:
    # Leer el archivo CSV
    df = pd.read_csv(archivo)

    # Convertir cps a rpm
    def cps_rpm(x):
        return x / 58200 * 60

    # Obtener los valores de x e y a partir de los datos
    x = df['Tiempo']
    y = df['V error'].apply(cps_rpm)  # Convertir los valores de "V error" de cps a rpm

    # Cálculo velocidad real
    for i in range(11, len(y)):
        y[i] = 50 - y[i]

    # Obtener el nombre del método a partir del diccionario
    nombre_metodo = nombres_metodos[archivo]

    # Obtener el color del método en el orden correcto
    color = next(color for method, color in zip(metodos, colores) if method == nombre_metodo)

    # Graficar los datos del archivo CSV actual con el nombre del método y color
    plt.plot(x, y, label=nombre_metodo, color=color)

# Configurar el aspecto de la gráfica
plt.xlim(0, 20)  # Establecer los límites del eje x
plt.ylim(0, 55)  # Establecer los límites del eje y
plt.xlabel('Tiempo')
plt.ylabel('Velocidad real (rpm)')
plt.title('Gráfico de velocidad real con valores nominales en función del tiempo')

# Crear etiquetas personalizadas para la leyenda con los métodos y colores correspondientes
etiquetas =[Line2D([0], [0], color=color, label=metodo) for metodo, color in zip(metodos, colores)]

# Configurar la leyenda con las etiquetas personalizadas
plt.legend(handles=etiquetas, loc='center left', bbox_to_anchor=(1, 0.5))

# Ajustar los márgenes para asegurar que la leyenda se muestra completamente
plt.subplots_adjust(right=0.8)
plt.grid()
plt.show()