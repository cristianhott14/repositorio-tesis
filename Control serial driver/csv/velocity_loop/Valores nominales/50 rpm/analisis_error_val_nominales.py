import numpy as np
import pandas as pd
import glob

# Convertir cps a rpm
def cps_rpm(x):
    return x/58200*60

# Buscar todos los archivos CSV en la carpeta actual
csv_files = glob.glob("*.csv")

# Ordenar la lista de archivos CSV en función del número en el nombre de archivo
csv_files = sorted(csv_files, key=lambda x: int(x.split()[0]))

Ise_data = []
Itse_data = []
Iae_data = []
Itae_data = []

for file in csv_files:
    #print(file)
    # Leer los datos desde un archivo csv
    df = pd.read_csv(file)

    # Obtener los valores de x e y a partir de los datos
    x = df['Tiempo']
    df['V error'] = df['V error'].apply(cps_rpm)      # conversion
    y = df['V error']
    #t = np.linspace(0,max(x),100)

    # Calcular tiempo de muestreo
    sum=0
    for i in range(0,len(x)-1):
        dif = x[i+1]-x[i]
        sum = dif + sum

    Ts = sum/len(x)/60
    #print("Ts: ",Ts)

    Ise = Itse = Iae = Itae = 0
    for j in range(11,len(y)):
        aux = abs((y[j] + y[j-1])/2 *Ts)
        Iae = Iae + aux
        aux2 = aux*j
        Itae = Itae + aux2
        aux3 = ((y[j])**2 + (y[j-1])**2)/2 *Ts
        Ise = Ise + aux3
        aux4 = aux3*j
        Itse = Itse + aux4
    
    # Almacenar lo valores de cada archivo
    Iae_data.append(Iae)
    Itae_data.append(Itae)
    Ise_data.append(Ise)
    Itse_data.append(Itse)

# Funcion para promediar cada n elementos
def promediar(lista):
    n = 3
    return [round(np.sum(lista[i:i+n])/n,3) for i in range(0, len(lista), n)]

Iae_data=promediar(Iae_data)
Itae_data=promediar(Itae_data)
Ise_data=promediar(Ise_data)
Itse_data=promediar(Itse_data)


# Ordenar resultados por nombre de metodo
def resultados(data):
    # Nombre de metodos
    metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova"]
    # Combinar las listas Ise_data y metodos en una lista de tuplas
    tuplas_data = list(zip(data, metodos))

    # Ordenar la lista de tuplas por el valor de Ise
    tuplas_ordenadas = sorted(tuplas_data, key=lambda tup: tup[0])
    #metodos_ordenados = [tupla[1] for tupla in tuplas_ordenadas]

    # Separar la lista ordenada en dos listas diferentes
    data_ordenado, metodos_ordenados = zip(*tuplas_ordenadas)

    # Obtener procentaje de error en referencia al metodo con mejor resultados para cada indice de desempeño
    error_porcentual_ordenado = []
    for valor_actual in data_ordenado:
        error_porcentual = round((valor_actual - data_ordenado[0]) / data_ordenado[0] * 100,1)
        error_porcentual_ordenado.append(error_porcentual)

    print("Metodos:",metodos_ordenados)
    print("Valor:",data_ordenado)
    print("Error %:",error_porcentual_ordenado,"\n")
    
print("IAE en orden")
Iae_orden = resultados(Iae_data)

print("ITAE en orden")
Itae_orden = resultados(Itae_data)

print("ISE en orden")
Ise_orden = resultados(Ise_data)

print("ITSE en orden")
Itse_orden = resultados(Itse_data)