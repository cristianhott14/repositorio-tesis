import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

# Obtener la lista de archivos CSV en las carpetas de 50 rpm y 150 rpm
csv_files_50rpm = glob.glob('50 rpm/*.csv')
csv_files_150rpm = glob.glob('150 rpm/*.csv')

# Ordenar las listas de archivos CSV en función del número en el nombre de archivo
csv_files_50rpm = sorted(csv_files_50rpm, key=lambda x: int(x.split()[0]))
csv_files_150rpm = sorted(csv_files_150rpm, key=lambda x: int(x.split()[0]))

# Nombre de métodos
metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova"]

# Paleta de colores personalizada
colores = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22']

# Asignar nombres de métodos a los archivos
nombres_metodos = {}
for i, archivo in enumerate(csv_files_50rpm + csv_files_150rpm):
    n_prueba = (archivo.split(" ")[1]).lstrip('rpm\ ')     # Obtener el 
    metodo = metodos[int(n_prueba)]  # Obtener el nombre de método correspondiente al numero de prueba
    nombres_metodos[archivo] = metodo

# Procesar cada archivo CSV
for archivo in csv_files_50rpm + csv_files_150rpm:
    # Obtener la velocidad de prueba del archivo
    v_prueba = int(archivo.split(" ")[0])

    # Leer el archivo CSV
    df = pd.read_csv(archivo)

    # Convertir cps a rpm
    def cps_rpm(x):
        return x / 58200 * 60

    # Obtener los valores de x e y a partir de los datos
    x = df['Tiempo']
    y = df['V error'].apply(cps_rpm)  # Convertir los valores de "V error" de cps a rpm

    # Cálculo velocidad real
    for i in range(11, len(y)):
        y[i] = v_prueba - y[i]

    # Obtener el nombre del método a partir del diccionario
    nombre_metodo = nombres_metodos[archivo]

    # Obtener el color del método en el orden correcto
    color = next(color for method, color in zip(metodos, colores) if method == nombre_metodo)

    # Graficar los datos del archivo CSV actual con el nombre del método y color
    plt.plot(x, y, label=nombre_metodo, color=color)

# Configurar el aspecto de la gráfica
plt.xlim(0, 20)  # Establecer los límites del eje x
plt.ylim(0, 160)  # Establecer los límites del eje y
plt.xlabel('Tiempo')
plt.ylabel('Velocidad real (rpm)')
plt.title('Gráfico de velocidad real con valores nominales en función del tiempo')

# Crear etiquetas personalizadas para la leyenda con los métodos y colores correspondientes
etiquetas = [Line2D([0], [0], color=color, label=metodo) for metodo, color in zip(metodos, colores)]

# Configurar la leyenda con las etiquetas personalizadas
plt.legend(handles=etiquetas, loc='center left', bbox_to_anchor=(1, 0.5))

# Ajustar los márgenes para asegurar que la leyenda se muestra completamente
plt.subplots_adjust(right=0.8)
plt.grid()
plt.show()