import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

# Obtener la lista de archivos CSV en las carpetas de 50 rpm y 150 rpm
csv_files_50rpm = glob.glob('50 rpm/*.csv')
csv_files_150rpm = glob.glob('150 rpm/*.csv')

# Ordenar las listas de archivos CSV en función del número en el nombre de archivo
csv_files_50rpm = sorted(csv_files_50rpm, key=lambda x: int(x.split()[0]))
csv_files_150rpm = sorted(csv_files_150rpm, key=lambda x: int(x.split()[0]))

# Nombre de métodos
metodos = ["CME", "Z-N", "Miller", "Smith", "Alfaro", "Broida", "Chen-Yang", "Ho", "Viteckova"]

# Procesar cada archivo CSV
for archivo in csv_files_50rpm + csv_files_150rpm:
    # Obtener la velocidad de prueba del archivo
    v_prueba = int(archivo.split(" ")[0])

    # Obtener el numero de prueba
    n_prueba = (archivo.split(" ")[1]).lstrip('rpm\ ')
    print(n_prueba)

    # Convertir cps a rpm
    def cps_rpm(x):
        return x/58200*60
    
    # Convertir a velocidad actual
    def v_actual(error,velocidad_prueba):
        return velocidad_prueba-error

    # Leer el archivo CSV
    df = pd.read_csv(archivo)

    # Obtener el nombre del método a partir del diccionario
    nombre_metodo = metodos[int(n_prueba)]

    # Obtener los valores
    x = df['Tiempo']
    # Conversion cps a rpm
    df['V error'] = df['V error'].apply(cps_rpm) 
    # Cálculo velocidad actual
    df['V actual'] = df.loc[df.index >= 27, 'V error'].apply(v_actual, args=(v_prueba,))

    # Extraer valores despues del sobreimpulso
    y = df.loc[27:,'V actual'].values

    # Calculo de tiempo de muestreo Ts
    sum=0
    for i in range(0,len(x)-1):
        dif = x[i+1]-x[i]
        sum = dif + sum
    Ts = sum/len(x)
    #print("Ts: ",Ts)

    N = len(y)  # tamano de datos
    frec = fftfreq(N, Ts)[:N//2]  # Calcular las frecuencias correspondientes a los coeficientes de Fourier
    fourier1 = fft(y)

    w = np.blackman(N)
    # Transformada de Fourier
    fourier2 = fft(y*w)

    plt.semilogy(frec[1:N//2], 2.0/N * np.abs(fourier1[1:N//2]), '-b')
    plt.semilogy(frec[1:N//2], 2.0/N * np.abs(fourier2[1:N//2]), '-r')
    #plt.xlim(0.1,4)
    plt.legend(['FFT', 'FFT w. window'])
    plt.title('Análisis espectral '+ str(v_prueba) + ' rpm método '+ nombre_metodo)
    plt.xlabel('Frecuencia (Hz)')
    plt.ylabel('Magnitud')
    # Configurar el tamaño de los ticks
    plt.gca().xaxis.set_major_locator(plt.MultipleLocator(0.2))
    plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(0.1))
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.tick_params(axis='both', which='minor', labelsize=9)
    plt.grid()
    plt.show()