import serial, time
import driver_func as dr
import csv_register

if __name__=="__main__":
    print("PRUEBA DE VELOCIDAD PARA OBTENER CORRIENTE COMANDO")

    # Se inicia la comunicacion por el puerto serial
    ser = serial.Serial('COM3', baudrate=9600, timeout= 0.1)
    print("Iniciando comunicacion con Drive")

    # Se configura comunicación con el drive
    dr.comm_ser(ser)

    # Establece la velocidad del motor en 0 cuentas/segundo
    dr.SendAsciiCmd(ser, 's r0x2f 0')

    # Ingreso de la velocidad de prueba y tiempo de prueba
    while True:
        try:
            vel_deseada_rpm = int(input("Ingresar velocidad de prueba entre -200 a 200 rpm: "))
            T_prueba_deseado = int(input("Ingresar tiempo de prueba en segundos: "))
            if vel_deseada_rpm >= -200 and vel_deseada_rpm <= 200 and T_prueba_deseado > 1:
                break
            else:
                print("Al menos uno de los valores no cumple con las condiciones. Intente de nuevo.")
        except ValueError:
            print("Error al ingresar un valor no válido. Intente de nuevo.")

    # Transformar rpm a cps. Al drive debe ser ingreado como 0.1 cps. Encoder con 5820 cuentas.
    vel_deseada_cps= str(int(vel_deseada_rpm/60*58200))
    #print("Velocidad en cps: ",vel_deseada_cps)

    # Habilitar el drive en el modo de velocidad programada 
    dr.SendAsciiCmd(ser, 's r0x24 11')

    # Se llama a la clase CSV para el registro en csv
    registrador = csv_register.CSV(vel_deseada_rpm,None,T_prueba_deseado, None,'current command')

    # Variable auxiliar para cambiar la velocidad del motor
    estado = False
    
    # Se inicia el tiempo de prueba
    T_prueba_inicio = time.time()

    try:
        while True:
            # Tiempo de prueba actual
            T_prueba_actual = round(time.time()-T_prueba_inicio,3)
            #print(T_prueba_actual) 

            # Corriente comando [0.01 A]
            I_comando = dr.SendAsciiCmd(ser, 'g r0x15')

            # Registro de tiempo de prueba y corriente comando en csv
            registrador.fill_csv_current_command(T_prueba_actual, I_comando)    

            # Condicion para cambiar la velocidad en el segundo 1
            if T_prueba_actual >= 1 and not estado:
                # Establece la velocidad del motor en cuentas/segundo
                dr.SendAsciiCmd(ser, 's r0x2f '+vel_deseada_cps)
                estado = True
            
            # Condición cuando se supere el timepo de prueba
            if T_prueba_actual >= 1 + T_prueba_deseado:
                # Establece la velocidad del motor en 0 cuentas/segundo. 
                dr.SendAsciiCmd(ser, 's r0x2f 0')

                # Se desactiva el drive
                dr.SendAsciiCmd(ser, 's r0x24 0')
                
                # Se cierra la comunicacion
                ser.close()
                print("Prueba finalizada.")
                break

    # Se puede interrumpir el programa con "ctrl+c"   
    except(KeyboardInterrupt,SystemExit):       
        # Se desactiva el drive
        dr.SendAsciiCmd(ser, 's r0x24 0') 
                
        # Se cierra la comunicacion
        ser.close()
        print("Prueba cancelada.")