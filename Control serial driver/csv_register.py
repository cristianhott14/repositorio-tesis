import os
import csv
import time

class CSV:
    def __init__(self, velocity, current, timetest, method, mode):
        """ Definir archivo CSV con los nombres de las columnas y dirreccion del archivo """

        # Variables de directorio y datos a muestrear
        self.recording_date = (time.ctime(time.time()).replace("  ", r" ").replace(":", r"-"))
        self.count = 0
        
        if mode == 'general':   # monitoreo general
            self.foldername = 'csv/general_monitoring/'
            self.directory_csv = self.foldername + str(velocity) + " rpm " + str(timetest) + "s " + self.recording_date + '.csv'
            self.fieldnames = ['i', 'Tiempo', 'V comando', 'V actual', 'I comando','I actual']
        
        if mode == 'current command':   # monitoreo corriente comando
            self.foldername = 'csv/current_command/'
            self.directory_csv = self.foldername + str(velocity) + " rpm " + str(timetest) + "s " + self.recording_date + '.csv'
            self.fieldnames = ['i', 'Tiempo', 'I comando']

        if mode == 'current loop':   # prueba lazo de corriente
            self.foldername = 'csv/current_loop/'
            self.directory_csv = self.foldername + str(current) + " A " + str(timetest) + "s " + self.recording_date + '.csv'
            self.fieldnames = ['i', 'Tiempo', 'V actual']
        
        if mode == 'velocity loop':   # prueba lazo de velocidad
            self.foldername = 'csv/velocity_loop/'
            self.directory_csv = self.foldername + str(method) + " " + str(velocity) + " rpm " + str(timetest) + "s " + self.recording_date + '.csv'
            self.fieldnames = ['i', 'Tiempo', 'V error']
        
        if mode == 'velocity loop2':   # prueba de velocidad
            self.foldername = 'csv/velocity_loop2/'
            self.directory_csv = self.foldername + str(timetest) + "s " + self.recording_date + '.csv'
            self.fieldnames = ['i', 'Tiempo', 'V actual']
        
        # Si la ruta de la carpeta no existe, se crea
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            print('Directorio creado:', self.foldername)

        # Crear archivo csv
        with open(self.directory_csv, mode='w', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)
            csv_file.writeheader()


    def fill_csv_general(self, Tiempo, V_comando, V_actual, I_comando, I_actual):
        """ Recopilacion datos, se rellena una fila del .csv """
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'i': self.count,
                   'Tiempo': Tiempo,
                   'V comando': V_comando[1:],
                   'V actual': V_actual[1:],
                   'I comando': I_comando[1:],
                   'I actual': I_actual[1:]
                   }
            csv_file.writerow(row)

    def fill_csv_current_command(self, Tiempo, I_comando):
        """ Recopilacion datos, se rellena una fila del .csv """
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'i': self.count,
                   'Tiempo': Tiempo,
                   'I comando': I_comando[1:]
                   }
            csv_file.writerow(row)
    

    def fill_csv_current_loop(self, Tiempo, V_actual):
        """ Recopilacion datos, se rellena una fila del .csv """   
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'i': self.count,
                   'Tiempo': Tiempo,
                   'V actual': V_actual[1:]
                   }
            csv_file.writerow(row)


    def fill_csv_velocity_loop(self, Tiempo, V_error):
        """ Recopilacion datos, se rellena una fila del .csv """
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'i': self.count,
                   'Tiempo': Tiempo,
                   'V error': V_error[1:]
                   }
            csv_file.writerow(row)   


    def fill_csv_velocity_loop2(self, Tiempo, V_actual):
        """ Recopilacion datos, se rellena una fila del .csv """
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'i': self.count,
                   'Tiempo': Tiempo,
                   'V actual': V_actual[1:],
                   }
            csv_file.writerow(row)