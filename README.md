# Repositorio tesis
## Descripción
Este repositorio contiene los códigos utilizados para configurar el ESP32 , códigos para realizar el control por comunicación serial del driver Xenus de Copley Controls, archivos de configuración del driver Xenus de Copley Controls, hojas de datos de resultados obtenidos en las pruebas y scripts para análisis de datos, contenidos en carpetas que se describen a continuación.

### Archivos excel citados
En el documento se citan los archivos excel (.xlsx) que contienen datos de las pruebas de ensayo de prueba NTC, promedio de resistencias NTC y resultados prueba circuito abierto, los cuales no pudieron ser agregados en anexos por su larga extensión.

### Códigos ESP
Esta carpeta contiene los archivos .ino que configuraron en el ESP32 para mediar la temperatura ambiente con el sensor DHT22, determinar la resistencia del termistor y para realizar un monitor web de las temperaturas del devanado del motor.

### Modelado NTC
Esta contiene los archivos .xlsx de los resultados de la caracterización del termistor. Además, contiene el archivo `calculadora_temp_termistor.py` que realiza el ajuste del comportamiento del termistor con los datos obtenidos para los puntos bajo y alto de temperatura.

### Configuraciones del driver
Esta carpeta contiene los archivos de configuración del driver .ccx utilizados en el software CME de Copley Controls para las configuraciones con valores originales, nominales y experimentales.

### Control serial driver
Esta carpeta contiene todos los códigos .py utilizados para realizar las pruebas mediante comunicación serial que se describen a continuación:
- `driver_func.py`: contienes las funciones utilizadas para configurar la comunicación serial con el driver, como también funciones para enviar mensajes ASCII y leer las respuesta del driver.
- `csv_register.py`: contiene la clase csv utilizada en los códigos de control del driver para almacenar los datos de tiempo y parámetro de interés en archivos csv.
- `control_dsp_data.py`: este ejecuta la prueba para obtener el valor de la corriente comando a una velocidad de prueba.
- `control_current_dsp.py`: este ejecuta la prueba para obtener la respuesta de la planta a una entrada escalón en forma de corriente comando.
- `control_velocity_dsp_data_PI.py`: ejecuta la prueba de rendimiento configurando las diferentes ganancias obtenidas para el lazo de control de velocidad.

En esta carpeta también se almacena la carpeta `csv` que contiene todos los resultados en archivos csv ordenados por carpetas. También existen scripts dentro de estas para realizar cálculos o visualización gráfica de los resultados.

Finalmente está el script `error_analysis.py` que realiza un análisis del error para todos los resultados obtenidos de la prueba de rendimiento.