import glob, os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Leer ensayo de prueba
Resistencia_ensayo= "Ensayo prueba NTC.xlsx"
dfe = pd.read_excel(Resistencia_ensayo)
resistencia = dfe['Resistencia']
temperatura = dfe['Temperatura']

plt.plot(temperatura,resistencia,label="Datos")
plt.legend(loc = "upper right")
plt.xlabel('Temperatura [\xb0C]')
plt.ylabel('Resistencia [ohm]')
plt.title('Resistencia del termistor v/s Temperatura del devanado')
plt.show()

# Leer el último archivo CSV de los promedios de la resistencia NTC
Resistencias_NTC = "Promedio de resistencias NTC.xlsx"

# Leer la primera hoja del archivo
dfb = pd.read_excel(Resistencias_NTC, sheet_name=0)

# Leer la segunda hoja del archivo
dfa = pd.read_excel(Resistencias_NTC, sheet_name=1)

# Obtener los valores
resistenciaB = dfb['ResistenciaBajo']
temperaturaB = dfb['TemperaturaBajo']
resistenciaA = dfa['ResistenciaAlto']
temperaturaA = dfa['TemperaturaAlto']

plt.plot(temperaturaB,resistenciaB,"b",label="Punto Bajo")
plt.plot(temperaturaA,resistenciaA,"r",label="Punto Alto")
plt.xlim(20,65)
plt.ylim(180,240)
plt.legend(loc = "upper right")
plt.xlabel('Temperatura [\xb0C]')
plt.ylabel('Resistencia [ohm]')
plt.title('Resistencia del termistor v/s Temperatura del devanado')
plt.show()


def temperatura(resistencia,temperatura):
    # Encontrar valores bajo,medio y alto de resistencia
    minR = resistencia[0]
    pos_medR = len(resistencia)//2+1
    medR= resistencia[pos_medR]
    maxR = resistencia[len(resistencia)-1]
    R1=minR
    R2=medR
    R3=maxR

    # Encontrar valores bajo,medio y alto de temperatura
    minT = temperatura[0]
    pos_medT = len(temperatura)//2+1
    medT= temperatura[pos_medT]
    maxT = temperatura[len(temperatura)-1]
    T1=minT+ 273.15
    T2=medT+ 273.15
    T3=maxT+ 273.15

    print("Resistencias: ",R1,R2,R3)
    print("Temperaturas: ",T1,T2,T3)

    # Definir la matriz M
    M = np.array([[1, np.log(R1), np.log(R1)**3],
                [1, np.log(R2), np.log(R2)**3],
                [1, np.log(R3), np.log(R3)**3]])

    # Definir el vector b
    b = np.array([1/T1, 1/T2, 1/T3])

    # Resolver el sistema de ecuaciones lineales Ax = b
    x = np.linalg.solve(M, b)

    # Imprimir la solución
    #print("La solución del sistema es:")
    #print(x)

    A = x[0]
    B = x[1]
    C = x[2]
    print("A:",A,"B:",B,"C:",C)

    T = np.round(1/(A+B*np.log(resistencia)+C*(np.log(resistencia))**3)-273.15,2)
    return T

print("Punto Bajo")
Temp_Ter_Bajo = temperatura(resistenciaB,temperaturaB)
print("Punto Alto")
Temp_Ter_Alto = temperatura(resistenciaA,temperaturaA)

plt.plot(temperaturaB,resistenciaB,"k",label="Datos obtenidos")
plt.plot(Temp_Ter_Bajo,resistenciaB,"b",label="Modelo S-H",linestyle = 'dashed')
plt.xlim(20,60)
plt.ylim(180,240)
plt.legend(loc = "upper right")
plt.xlabel('Temperatura [\xb0C]')
plt.ylabel('Resistencia [ohm]')
plt.title('Resistencia v/s Temperatura (punto más bajo)')
plt.show()

plt.plot(temperaturaA,resistenciaA,"k",label="Datos obtenidos")
plt.plot(Temp_Ter_Alto,resistenciaA,"r",label="Modelo S-H",linestyle = 'dashed')
plt.xlim(20,65)
plt.ylim(180,240)
plt.legend(loc = "upper right")
plt.xlabel('Temperatura [\xb0C]')
plt.ylabel('Resistencia [ohm]')
plt.title('Resistencia v/s Temperatura (punto más alto)')
plt.show()