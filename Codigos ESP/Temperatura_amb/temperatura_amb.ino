#include <DHT.h>

// DHT22
#define DHTTYPE  DHT22  // Definimos el modelo del sensor DHT22
#define DHTPIN    4     // Se define el pin D4 del ESP32 para conectar el sensor DHT22
DHT dht(DHTPIN, DHTTYPE);

long Tiempo_Ult_Lect=0; //Para guardar el tiempo de la última lectura
int i=0;

void setup() {
  Serial.begin(115200);

  // Iniciar DHT22
  dht.begin();
}

void loop() {
  if(millis()-Tiempo_Ult_Lect>2000){    // Diferencia de 2 seg
    // Indice de medicion
    Serial.print(i);
    Serial.print(",");
    i+=1;
    
    // Se lee la temperatura en grados Celsius por defecto
    float t = dht.readTemperature(); 
    Serial.print("Temperatura ambiente: ");
    Serial.print(t,2);

    //Actualizacion del tiempo de la última lectura
    Tiempo_Ult_Lect=millis(); 

  }
  
}
