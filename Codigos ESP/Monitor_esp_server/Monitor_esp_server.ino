#include <ESPAsyncWebSrv.h>
#include "SPIFFS.h"
#include <Adafruit_ADS1X15.h>
#include <math.h>

// ADS1115
Adafruit_ADS1115 ads;  // Crear objeto de la clase
float factorEscala = 0.03125F; // Factor de escala de 0.03125mV

// Valores para calulo de resistencia
float R1 = 1000;
float R2 = 1001;
float R3 = 237;
float Vo = 3.3;
float Vab;
float Rt;
int16_t valor_adc;
int i = 0;
float suma_rt;
float promedio_rt;
float temp_b, temp_a;

//Coeficientes de S-H punto bajo
float Ab = -0.06485327360280073;
float Bb =  0.018430425591997947;
float Cb = -0.0001989248356948248;

//Coeficientes de S-H punto alto
float Aa = -0.0817602027233843;
float Ba =  0.023068145406514938;
float Ca = -0.00025063773331893585;

// Inregesar credenciales de red
const char* ssid = "wifi_lab";
const char* password = "8=1Y229z";
// Crear obejto AsyncWebServer en el puerto 80
AsyncWebServer server(80);

//Funcion para redondear float
float redondear(float valor, int decimales){
  long expo= 1;
  if (decimales > 0){
    for(int i=1; i<=decimales; i++){
      expo *=10;
    }
  }
  float aux= (long)(valor*expo+0.5)/(float)expo;
  return aux;
}

String Temperatura() {
    suma_rt=0;
    for (int a=0; a < 20; a++){
      // Calcular temperatura segun resistencia
      // Lectura ADS
      valor_adc = ads.readADC_Differential_0_1();
      Vab = valor_adc * factorEscala/1000.0; // Conversion a volts

      // Calculo de Rt
      Rt=-((Vab/Vo)*(R1*R2+R2*R3)-(R2*R3))/(R1+(Vab/Vo)*(R1+R3)); //calculo Rt
      suma_rt = Rt + suma_rt;
      delay(100);
    }
    promedio_rt= suma_rt/20;
    Serial.print("Promedio: ");
    Serial.println(promedio_rt);

    // Calculo de temperaturas
    float logR= log(promedio_rt);
    // Punto mas bajo
    temp_b = 1/(Ab+Bb*logR+Cb*logR*logR*logR)-273.15;
    temp_b = redondear(temp_b,1);

    // Punto mas alto
    temp_a = 1/(Aa+Ba*logR+Ca*logR*logR*logR)-273.15;
    temp_a = redondear(temp_a,1);
    
    Serial.println(temp_b);
    Serial.println(temp_a);
    i=i+1;
    
    String str1 = String(temp_b,1);
    String str2 = String(temp_a,1);
    
    return String(str1+","+str2);
}

void setup() {
  // Iniciar comunicacion
  Serial.begin(115200);

  // Inicializa SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("A ocurrido un error al montar SPIFFS");
    return;
  }

  // Conectar a Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando a WiFi..");
  }

  // Imprimir direccion IP local del ESP32
  Serial.println(WiFi.localIP());

  // Ruta para la pagina web principal
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  // Ruta para obtener las temperaturas
  server.on("/TempDev", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", Temperatura().c_str());
  });

  // Factor de ganancia ADC
  ads.setGain(GAIN_FOUR);  // Ganancia de x4, +/- 1.024V  1 bit = 0.03125mV //GAIN_FOUR

  // Iniciar el ADS1115
  if (!ads.begin()) {
    Serial.println("Fallo al inicializar ADS");
    while (1);}
    
  // Inicio del servidor
  server.begin();
}

void loop() {
}
